const API_KEY =
  "3fb2ae016424cfae9d82f5a5d90cd62f631a6c8b4e68493f8035d70fae655bbf";

const AGGREGATE_INDEX = "5";
const invalidTickers = [];
const tickersHandlers = new Map(); // {}
const socket = new WebSocket(
  `wss://streamer.cryptocompare.com/v2?api_key=${API_KEY}`
);

socket.addEventListener("message", (e) => {
  const {
    TYPE: type,
    FROMSYMBOL: currency,
    PRICE: newPrice,
    MESSAGE: status,
    PARAMETER: parameter,
  } = JSON.parse(e.data);

  //console.log(JSON.parse(e.data));
   
  if(status  === "INVALID_SUB") invalidTickers.push(parameter.replace("5~CCCAGG~", "").replace("~USD", ""));
  
  
  if (type !== AGGREGATE_INDEX || newPrice === undefined) {
    return;
  }

  const handlers = tickersHandlers.get(currency) ?? [];

  handlers.forEach((fn) => fn(newPrice, status));
});

function sendToWebSocket(message) {
  const stringifiedMessage = JSON.stringify(message); // преобразуем в JSON

  if (socket.readyState === WebSocket.OPEN) {
    // если состояние WS «OPEN»: обмен данными , отправляем сообщение с подпиской на монету
    socket.send(stringifiedMessage);
    return;
  }

  socket.addEventListener(
    // иначе ждем события open и отправляем с ключем  once: true, те один раз и удалится
    "open",
    () => {
      socket.send(stringifiedMessage);
    },
    { once: true }
  );
}

function subscribeToTickerOnWs(ticker) {
  sendToWebSocket({
    // которая вызывает отправку в WS данных монеты на которую нужно подписаться
    action: "SubAdd",
    subs: [`5~CCCAGG~${ticker}~USD`],
  });
}

function unsubscribeFromTickerOnWs(ticker) {
  sendToWebSocket({
    action: "SubRemove",
    subs: [`5~CCCAGG~${ticker}~USD`],
  });
}

export const subscribeToTicker = (ticker, cb) => {
  const subscribers = tickersHandlers.get(ticker) || []; // мы получаем из Map значение по ключу (ticker) или пустой массив
  tickersHandlers.set(ticker, [...subscribers, cb]); // в Map мы записываем или перезаписываем на ключ  (ticker) значение массив предыдущих колбеков + новый колбек
  subscribeToTickerOnWs(ticker); // и вызываем подписку на тикер
};

export const unsubscribeFromTicker = (ticker) => {
  tickersHandlers.delete(ticker);
  unsubscribeFromTickerOnWs(ticker);
};

export const getInvalidTickers = () => {
  return invalidTickers; 
};
